import { IBaseErrorProps } from './base/BaseError';
import { CoreError } from './CoreError';

export class AppError extends CoreError {
    constructor(props?: IBaseErrorProps) {
        super(props);
        this.name = 'AppError';
    }
}