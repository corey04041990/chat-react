import { IDictionary } from '../../IDictionary';

export type IBaseErrorData = IDictionary<any>;

export interface IBaseErrorProps {
    message?: string;
    status?: number;
    code?: string;
    data?: IBaseErrorData;
}

const prepareProps = (props?: unknown): IBaseErrorProps => {
    if (props && props instanceof BaseError) {
        return props;
    }

    if (props && typeof props === 'string') {
        return { message: props };
    }

    return (props || {}) as IBaseErrorProps;
}

export abstract class BaseError extends Error {
    message: string;
    status: number;
    code: string;
    data?: IBaseErrorData;

    protected constructor(props?: IBaseErrorProps) {
        props = prepareProps(props);
        const message = props.message || 'An unexpected error occurred.';

        super(message);

        this.name = 'BaseError';
        this.message = message;
        this.status = props.status || 500;
        this.code = props.code || 'UNEXPECTED_ERROR';

        if (props.data) {
            this.data = props.data;
        }
    }

    toJSON() {
        const json: IDictionary<any> = {
            name: this.name,
            status: this.status,
            code: this.code,
            message: this.message,
        };

        if (this.data) {
            json.data = this.data;
        }

        return json;
    }

    toString() {
        return JSON.stringify(this.toJSON());
    }
}
