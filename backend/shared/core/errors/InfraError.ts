import { BaseError, IBaseErrorProps } from './base/BaseError';

export class InfraError extends BaseError {
    constructor(props?: IBaseErrorProps) {
        super(props);
        this.name = 'InfraError';
    }
}