import { BaseError, IBaseErrorProps } from './base/BaseError';

export class CoreError extends BaseError {
    constructor(props?: IBaseErrorProps) {
        super(props);
        this.name = 'CoreError';
    }
}