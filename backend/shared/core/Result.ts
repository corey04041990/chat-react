import { BaseError, CoreError } from './errors';

export type Either<L, A> = Left<L, A> | Right<L, A>;

export class Left<L, A> {
    readonly value: L;

    constructor(value: L) {
        this.value = value;
    }

    isLeft(): this is Left<L, A> {
        return true;
    }

    isRight(): this is Right<L, A> {
        return false;
    }
}

export class Right<L, A> {
    readonly value: A;

    constructor(value: A) {
        this.value = value;
    }

    isLeft(): this is Left<L, A> {
        return false;
    }

    isRight(): this is Right<L, A> {
        return true;
    }
}

export const left = <L, A>(l: L): Either<L, A> => {
    return new Left<L, A>(l);
};

export const right = <L, A>(a: A): Either<L, A> => {
    return new Right<L, A>(a);
};

export class Result<T> {
    isSuccess: boolean;
    isFailure: boolean;
    readonly _value: T;

    constructor(isSuccess: boolean, error?: T | null, value?: T) {
        if (isSuccess && error) {
            throw new CoreError({ message: 'A result cannot be successful and contain an error' });
        }

        if (!isSuccess && !error) {
            throw new CoreError({ message: 'A failing result needs to contain an error message' });
        }

        this.isSuccess = isSuccess;
        this.isFailure = !isSuccess;

        if (isSuccess) {
            this._value = value || ('Ok!' as unknown as T);
        } else {
            this._value = error || ('Unknown error result!' as unknown as T);
        }

        Object.freeze(this);
    }

    getValue() {
        return this._value;
    }

    getError<U extends BaseError | string | undefined>(): U {
        return this._value as unknown as U;
    }

    static ok<U>(value?: U): Result<U> {
        return new Result<U>(true, null, value);
    }

    static fail<U>(error: string | boolean | BaseError | U): Result<U> {
        return new Result<U>(false, error as U);
    }

    static combine(results: Result<any>[]): Result<any> {
        for (const result of results) {
            if (result.isFailure) {
                return result;
            }
        }

        return Result.ok();
    }

    static any(results: Result<any>[]): Result<any> {
        if (!results.length) {
            return Result.ok();
        }

        let firstFailed;

        for (const result of results) {
            if (!firstFailed && result.isFailure) {
                firstFailed = result;
            }

            if (result.isSuccess) {
                return result;
            }
        }

        return firstFailed as Result<any>;
    }
}