import { AppError, BaseError } from './errors';
import { Result } from './Result';

export namespace AppErrorResults {
    export class UnexpectedError extends Result<AppError> {
        constructor(err?: any) {
            if (!(err instanceof BaseError)) {
                err = new AppError(err);
            }

            super(false, err);
        }
    }

    export class UserNotFoundError extends Result<AppError> {
        constructor(id?: number | string) {
            super(false, new AppError({
                message: `User not found`,
                status: 404,
                code: 'USER_NOT_FOUND',
                data: { user_id: id }
            }));
        }
    }

    export class ValidationError extends Result<AppError> {
        constructor(message?: string) {
            super(false, new AppError({
                message: message || 'Validation error',
                status: 400,
                code: 'VALIDATION_ERROR',
            }));
        }
    }

    export class AccessError extends Result<AppError> {
        constructor(message?: string) {
            super(false, new AppError({
                message: message || 'The user does not have access to the source',
                status: 403,
                code: 'NO_ACCESS',
            }));
        }
    }
}
