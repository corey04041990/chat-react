export interface IDictionary<T = any> {
    [index: string | number]: T;
}