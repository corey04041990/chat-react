import { Result } from "../../../shared/core/Result";
import UserEntity from "./UserEntity";
import TransactionInfoEntity from "./TransactionInfoEntity";

interface IMessageProps {
    message: string;
    user: UserEntity;
    transactionInfo?: TransactionInfoEntity;
}

export default class MessageEntity {
    private props: IMessageProps;

    get user() {
        return this.props.user;
    }

    get message() {
        return this.props.message;
    }

    get transactionInfo() {
        return this.props.transactionInfo;
    }

    constructor(props: IMessageProps) {
        this.props = props;
    }

    static create(props: IMessageProps): Result<MessageEntity> {
        const entity = new MessageEntity(props);
        return Result.ok<MessageEntity>(entity);
    }
}
