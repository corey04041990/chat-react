import { Result } from "../../../shared/core/Result";

interface ITransactionInfoProps {
    txid: string;
    value: string;
    walletFrom: string;
    walletTo: string;
}

export default class TransactionInfoEntity {
    private props: ITransactionInfoProps;

    get txid() {
        return this.props.txid;
    }

    get value() {
        return this.props.value;
    }

    get walletFrom() {
        return this.props.walletFrom;
    }

    get walletTo() {
        return this.props.walletTo;
    }

    constructor(props: ITransactionInfoProps) {
        this.props = props;
    }

    static create(props: ITransactionInfoProps): Result<TransactionInfoEntity> {
        const entity = new TransactionInfoEntity(props);
        return Result.ok<TransactionInfoEntity>(entity);
    }
}
