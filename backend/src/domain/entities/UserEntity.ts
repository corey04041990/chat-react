import { Result } from "../../../shared/core/Result";

interface IUserProps {
    id: string;
    name: string;
    roomId: string;
}

export default class UserEntity {
    private props: IUserProps;

    get id() {
        return this.props.id;
    }

    get name() {
        return this.props.name;
    }

    get roomId() {
        return this.props.roomId;
    }

    constructor(props: IUserProps) {
        this.props = props;
    }

    static create(props: IUserProps): Result<UserEntity> {
        const entity = new UserEntity(props);
        return Result.ok<UserEntity>(entity);
    }
}
