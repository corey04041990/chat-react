import { AddUserDTO } from "../../use-cases/add-user/AddUserDTO";
import UserEntity from "../../../domain/entities/UserEntity";

export interface IUser extends AddUserDTO {
    id: string;
}

export interface IUserRepo {
    getUserByName(props: AddUserDTO): UserEntity | null;
    getUserById(userId: string): UserEntity | null;
    addUser(props: AddUserDTO): UserEntity | null;
    countUsersInCurrentRoom(roomId: string): number | null;
    removeUser(userId: string): boolean;
}
