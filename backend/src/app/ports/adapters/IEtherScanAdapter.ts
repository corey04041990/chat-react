export interface IPreparedTransaction {
    txid: string,
    value: string,
    walletFrom: string,
    walletTo: string
}

export interface IEtherScanAdapter {
    isHexString(value: string): boolean;
    getTransactionByHash(txHash: string): Promise<IPreparedTransaction>
}