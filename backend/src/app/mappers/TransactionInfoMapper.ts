import TransactionInfoEntity from "../../domain/entities/TransactionInfoEntity";
import { IPreparedTransaction } from "../ports/adapters/IEtherScanAdapter";

export class TransactionInfoMapper {
    static toDomain(raw: any): TransactionInfoEntity | null {
        const entityResult = TransactionInfoEntity.create(
            {
                txid: raw.txid,
                value: raw.value,
                walletFrom: raw.walletFrom,
                walletTo: raw.walletTo,
            },
        );

        return raw && entityResult.isSuccess ? entityResult.getValue() : null;
    }

    static toDto(entity: TransactionInfoEntity): IPreparedTransaction {
        return {
            txid: entity.txid,
            value: entity.value,
            walletFrom: entity.walletFrom,
            walletTo: entity.walletTo,
        };
    }
}