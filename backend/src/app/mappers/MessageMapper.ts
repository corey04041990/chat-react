import MessageEntity from '../../domain/entities/MessageEntity';
import { SendMessageDTO } from '../use-cases/send-message/SendMessageDTO';
import { UserMapper } from './UserMapper';
import { TransactionInfoMapper } from "./TransactionInfoMapper";

export class MessageMapper {
    static toDomain(raw: any): MessageEntity | null {
        const entityResult = MessageEntity.create(
            {
                message: raw.message,
                user: raw.user,
                transactionInfo: raw.transactionInfo && TransactionInfoMapper.toDomain(raw.transactionInfo) || undefined,
            },
        );

        return raw && entityResult.isSuccess ? entityResult.getValue() : null;
    }

    static toDto(entity: MessageEntity): SendMessageDTO {
        const dto: SendMessageDTO = {
            message: entity.message,
            user: UserMapper.toDto(entity.user),
        };

        if (entity.transactionInfo) {
            dto.transactionInfo = TransactionInfoMapper.toDto(entity.transactionInfo);
        }
        return dto;
    }
}