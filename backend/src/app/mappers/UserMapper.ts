import UserEntity from "../../domain/entities/UserEntity";
import { IUser } from "../ports/repos/IUserRepo";

export class UserMapper {
    static toDomain(raw: any): UserEntity | null {
        const entityResult = UserEntity.create({
            id: raw.id,
            name: raw.name,
            roomId: raw.roomId,
        });

        return raw && entityResult.isSuccess ? entityResult.getValue() : null;
    }

    static toDomainBulk(rawItems: any[]) {
        const entities: UserEntity[] = [];

        for (const rawItem of rawItems || []) {
            const entity = this.toDomain(rawItem);
            if (entity) {
                entities.push(entity);
            }
        }

        return entities;
    }

    static toDto(entity: UserEntity): IUser {
        return {
            id: entity.id,
            name: entity.name,
            roomId: entity.roomId,
        };
    }

    static toDTOBulk(entities: UserEntity[]): IUser[] {
        return (entities || []).map((item) => UserMapper.toDto(item));
    }
}
