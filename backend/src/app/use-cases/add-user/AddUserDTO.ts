export interface AddUserDTO {
    name: string;
    roomId: string;
}