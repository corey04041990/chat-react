import AddUserUseCase from "./AddUserUseCase";
import serviceConfig from "../../../config/serviceConfig";

const addUserUseCase = new AddUserUseCase(serviceConfig.repos.userRepo);

export { addUserUseCase };
