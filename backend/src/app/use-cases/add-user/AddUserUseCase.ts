import { AddUserDTO } from "./AddUserDTO";
import { IUser, IUserRepo } from "../../ports/repos/IUserRepo";
import { Either, left, Result, right } from "../../../../shared/core/Result";
import { AddUserErrorResults } from "./AddUserErrorResults";
import { AppErrorResults } from "../../../../shared/core/AppErrorResults";
import { BaseError } from "../../../../shared/core/errors";
import { UserMapper } from "../../mappers/UserMapper";

type Response = Either<Result<BaseError>, Result<IResult>>;

interface IResult {
    addedUser: IUser;
    cntUsersInCurrentRoom?: number;
}

export default class AddUserUseCase {
    private userRepo: IUserRepo;

    constructor(userRepo: IUserRepo) {
        this.userRepo = userRepo;
    }

    execute(dto: AddUserDTO): Response {
        try {
            if (!dto.name || !dto.roomId) {
                return left(new AddUserErrorResults.RequiredParams());
            }

            const user = this.userRepo.getUserByName(dto);

            const result: IResult = {
                addedUser: {} as IUser,
                cntUsersInCurrentRoom: 0,
            };

            if (user) {
                result.addedUser = UserMapper.toDto(user);
            } else {
                const addedUser = this.userRepo.addUser(dto);

                if (!addedUser) {
                    return left(new AppErrorResults.UnexpectedError());
                }

                result.addedUser = UserMapper.toDto(addedUser);
            }

            const cntUsersInCurrentRoom = this.userRepo.countUsersInCurrentRoom(
                dto.roomId,
            );

            if (cntUsersInCurrentRoom) {
                result.cntUsersInCurrentRoom = cntUsersInCurrentRoom;
            }

            return right(Result.ok<IResult>(result));
        } catch (e: any) {
            return left(new AppErrorResults.UnexpectedError(e));
        }
    }
}
