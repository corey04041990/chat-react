import { AppError } from "../../../../shared/core/errors/AppError";
import { Result } from "../../../../shared/core/Result";

export namespace AddUserErrorResults {
    export class RequiredParams extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "name and roomId are required",
                    status: 500,
                }),
            );
        }
    }

    export class UserExists extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "user already exists",
                    status: 500,
                }),
            );
        }
    }
}
