import RemoveUserUseCase from './RemoveUserUseCase';
import serviceConfig from '../../../config/serviceConfig';

const removeUserUseCase = new RemoveUserUseCase(serviceConfig.repos.userRepo);

export { removeUserUseCase };