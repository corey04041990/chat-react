import { AppError } from "../../../../shared/core/errors";
import { Result } from "../../../../shared/core/Result";

export namespace RemoveUserErrorResults {
    export class UserNotFound extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "user not found",
                    status: 500,
                }),
            );
        }
    }
}
