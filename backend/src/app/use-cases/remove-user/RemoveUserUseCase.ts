import { IUser, IUserRepo } from "../../ports/repos/IUserRepo";
import { RemoveUserDTO } from "./RemoveUserDTO";
import { Either, left, Result, right } from "../../../../shared/core/Result";
import { RemoveUserErrorResults } from "./RemoveUserErrorResults";
import { UserMapper } from "../../mappers/UserMapper";
import { BaseError } from "../../../../shared/core/errors";
import { AppErrorResults } from "../../../../shared/core/AppErrorResults";

type Response = Either<Result<BaseError>, Result<IResult>>;

export interface IResult {
    removedUser: IUser;
    cntUsersInCurrentRoom?: number;
}

export default class RemoveUserUseCase {
    private userRepo: IUserRepo;

    constructor(userRepo: IUserRepo) {
        this.userRepo = userRepo;
    }

    execute(dto: RemoveUserDTO): Response {
        try {
            const user = this.userRepo.getUserById(dto.id);

            if (!user) {
                return left(new RemoveUserErrorResults.UserNotFound());
            }

            this.userRepo.removeUser(user.id);

            const result: IResult = {
                removedUser: UserMapper.toDto(user),
            };

            const cntUsersInCurrentRoom = this.userRepo.countUsersInCurrentRoom(
                user.roomId,
            );

            if (cntUsersInCurrentRoom) {
                result.cntUsersInCurrentRoom = cntUsersInCurrentRoom;
            }

            return right(Result.ok<IResult>(result));
        } catch (e: any) {
            return left(new AppErrorResults.UnexpectedError(e));
        }
    }
}
