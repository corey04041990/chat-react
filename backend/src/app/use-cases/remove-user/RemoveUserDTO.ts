export interface RemoveUserDTO {
    id: string;
    name: string;
    roomId: string;
}