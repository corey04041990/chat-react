import SendMessageUseCase from "./SendMessageUseCase";
import serviceConfig from "../../../config/serviceConfig";

const sendMessageUseCase = new SendMessageUseCase(
    serviceConfig.repos.userRepo,
    serviceConfig.adapters.etherScanAdapter,
);

export { sendMessageUseCase };
