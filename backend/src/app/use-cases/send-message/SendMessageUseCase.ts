import { IUserRepo } from "../../ports/repos/IUserRepo";
import { Either, left, Result, right } from "../../../../shared/core/Result";
import { BaseError } from "../../../../shared/core/errors";
import { SendMessageDTO } from "./SendMessageDTO";
import { SendMessageErrorResults } from "./SendMessageErrorResults";
import { AppErrorResults } from "../../../../shared/core/AppErrorResults";
import { MessageMapper } from "../../mappers/MessageMapper";
import {
    IEtherScanAdapter,
    IPreparedTransaction,
} from "../../ports/adapters/IEtherScanAdapter";

type Response = Either<Result<BaseError>, Result<SendMessageDTO>>;

export default class SendMessageUseCase {
    private userRepo: IUserRepo;
    private etherScanAdapter: IEtherScanAdapter;

    constructor(userRepo: IUserRepo, etherScanAdapter: IEtherScanAdapter) {
        this.userRepo = userRepo;
        this.etherScanAdapter = etherScanAdapter;
    }

    async execute(dto: SendMessageDTO): Promise<Response> {
        try {
            if (!(dto.message || dto.user.id)) {
                return left(new SendMessageErrorResults.RequiredParams());
            }

            const user = this.userRepo.getUserById(dto.user.id);

            if (!user) {
                return left(new SendMessageErrorResults.UserNotExists());
            }

            const isMessageTxHash = this.etherScanAdapter.isHexString(
                dto.message,
            );
            let transactionInfo: IPreparedTransaction | null = null;

            if (isMessageTxHash) {
                transactionInfo =
                    await this.etherScanAdapter.getTransactionByHash(
                        dto.message,
                    );
            }

            const messageResult = MessageMapper.toDomain({
                message: dto.message,
                user,
                transactionInfo,
            });

            if (!messageResult) {
                return left(new SendMessageErrorResults.MessageNotCreated());
            }

            return right(
                Result.ok<SendMessageDTO>(MessageMapper.toDto(messageResult)),
            );
        } catch (e: any) {
            return left(new AppErrorResults.UnexpectedError(e));
        }
    }
}
