import { AppError } from "../../../../shared/core/errors";
import { Result } from "../../../../shared/core/Result";

export namespace SendMessageErrorResults {
    export class RequiredParams extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "name and roomId are required",
                    status: 500,
                }),
            );
        }
    }

    export class UserNotExists extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "user is not exists",
                    status: 500,
                }),
            );
        }
    }

    export class MessageNotCreated extends Result<AppError> {
        constructor() {
            super(
                false,
                new AppError({
                    message: "message not created",
                    status: 500,
                }),
            );
        }
    }
}
