import { IUser } from "../../ports/repos/IUserRepo";
import { IPreparedTransaction } from "../../ports/adapters/IEtherScanAdapter";

export interface SendMessageDTO {
    message: string;
    user: IUser;
    transactionInfo?: IPreparedTransaction;
}