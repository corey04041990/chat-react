import dotenv from 'dotenv';
dotenv.config({ path: './env/.env' });
import '../infra/express';
