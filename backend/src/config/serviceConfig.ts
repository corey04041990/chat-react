import { userRepo } from "../infra/data-base/repos";
import { etherScanAdapter } from "../infra/ether-scan-adapter";

export default {
    repos: {
        userRepo,
    },
    adapters: {
        etherScanAdapter,
    },
};
