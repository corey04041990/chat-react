import express from "express";
import http from "http";
import cors from "cors";

const app = express();
import { Server } from "socket.io";
import { socketService } from "./socket";

app.use(cors({ origin: "*" }));

const httpServer = http.createServer(app);
const io = new Server(httpServer, { cors: { origin: "*" } });

io.on("connection", (socket) => {
    socketService.joinRoom(socket);
    socketService.sendMessage(socket, io);
    socketService.leftUser(socket);

    socket.on("disconnect", () => {
        console.log("user disconnected");
    });
});

const PORT = process.env.APP_SERVE_PORT
    ? parseInt(process.env.APP_SERVE_PORT, 10)
    : 5000;
httpServer.listen(PORT, () => {
    console.log("Server is running on http://localhost:5000");
});
