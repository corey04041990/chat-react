import { Server, Socket } from "socket.io";
import { DefaultEventsMap } from "socket.io/dist/typed-events";
import { addUserUseCase } from "../../../app/use-cases/add-user";
import { sendMessageUseCase } from "../../../app/use-cases/send-message";
import { removeUserUseCase } from "../../../app/use-cases/remove-user";
import { IUser } from "../../../app/ports/repos/IUserRepo";

interface IParams {
    user: IUser;
    cntUsersInCurrentRoom?: number;
}

export default class SocketService {
    joinRoom(socket: Socket<DefaultEventsMap, DefaultEventsMap>) {
        socket.on("join", ({ name, roomId }) => {
            socket.join(roomId);

            const result = addUserUseCase.execute({ name, roomId });

            if (result.isRight()) {
                const { cntUsersInCurrentRoom, addedUser } =
                    result.value.getValue();

                socket.emit("message", {
                    data: {
                        user: { name: "Admin" },
                        message: `${name}, welcome to the room ${roomId}`,
                    },
                });

                this.updateUsers(socket, {
                    user: addedUser,
                    cntUsersInCurrentRoom,
                });

                this.sendSystemMessageToAllUsers(
                    socket,
                    addedUser,
                    `${addedUser.name} has joined!`,
                );
            }
        });
    }

    sendMessage(
        socket: Socket<DefaultEventsMap, DefaultEventsMap>,
        io: Server<DefaultEventsMap, DefaultEventsMap>,
    ) {
        socket.on("sendMessage", async ({ userMessage, loggedUser }) => {
            const result = await sendMessageUseCase.execute({
                message: userMessage,
                user: loggedUser,
            });
            if (result.isRight()) {
                const messageDTO = result.value.getValue();
                const { user, message, transactionInfo } = messageDTO;

                io.to(user.roomId).emit("message", {
                    data: {
                        user: { name: user.name },
                        message,
                        transactionInfo,
                    },
                });
            }
        });
    }

    leftUser(socket: Socket<DefaultEventsMap, DefaultEventsMap>) {
        socket.on("leftRoom", ({ loggedUser }) => {
            const result = removeUserUseCase.execute(loggedUser);

            if (result.isRight()) {
                const { cntUsersInCurrentRoom, removedUser } =
                    result.value.getValue();

                this.updateUsers(socket, {
                    user: removedUser,
                    cntUsersInCurrentRoom,
                });

                this.sendSystemMessageToAllUsers(
                    socket,
                    removedUser,
                    `${removedUser.name} has left!`,
                );
            }
        });
    }

    updateUsers(
        socket: Socket<DefaultEventsMap, DefaultEventsMap>,
        params: IParams,
    ) {
        socket.broadcast.to(params.user.roomId).emit("updateUsers", {
            data: {
                users: params.cntUsersInCurrentRoom,
            },
        });

        socket.emit("updateUsers", {
            data: {
                loggedUser: params.user,
                users: params.cntUsersInCurrentRoom,
            },
        });
    }

    sendSystemMessageToAllUsers(
        socket: Socket<DefaultEventsMap, DefaultEventsMap>,
        user: IUser,
        message: string,
    ) {
        socket.broadcast.to(user.roomId).emit("message", {
            data: {
                user: { name: "Admin" },
                message,
            },
        });
    }
}
