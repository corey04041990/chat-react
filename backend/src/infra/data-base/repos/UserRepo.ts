import { AddUserDTO } from "../../../app/use-cases/add-user/AddUserDTO";
import { InfraError } from "../../../../shared/core/errors";
import { UserMapper } from "../../../app/mappers/UserMapper";
import UserEntity from "../../../domain/entities/UserEntity";
import { IUser, IUserRepo } from "../../../app/ports/repos/IUserRepo";

export default class UserRepo implements IUserRepo {
    private users: IUser[];

    constructor() {
        this.users = [];
    }

    getUserByName(props: AddUserDTO): UserEntity | null {
        try {
            const result = this.users.find(
                (user: AddUserDTO) =>
                    user.name === props.name && user.roomId === props.roomId,
            );
            return (result && UserMapper.toDomain(result)) || null;
        } catch (e: any) {
            throw new InfraError(e);
        }
    }

    getUserById(userId: string): UserEntity | null {
        try {
            const result = this.users.find((user: IUser) => user.id === userId);
            return (result && UserMapper.toDomain(result)) || null;
        } catch (e: any) {
            throw new InfraError(e);
        }
    }

    countUsersInCurrentRoom(roomId: string): number | null {
        try {
            const users = this.users.filter(
                (user: IUser) => user.roomId === roomId,
            );
            return users.length ? users.length : null;
        } catch (e: any) {
            throw new InfraError(e);
        }
    }

    addUser(props: AddUserDTO): UserEntity | null {
        try {
            const user = {
                id: Date.now().toString(),
                name: props.name,
                roomId: props.roomId,
            };

            this.users.push(user);
            return UserMapper.toDomain(user);
        } catch (e: any) {
            throw new InfraError(e);
        }
    }

    removeUser(userId: string): boolean {
        try {
            this.users = this.users.filter((user) => user.id !== userId);
            return true;
        } catch (e: any) {
            throw new InfraError(e);
        }
    }
}
