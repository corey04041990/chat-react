import { fetch } from "undici";
import { ethers } from "ethers";
import { InfraError } from "../../../shared/core/errors";
import { IPreparedTransaction } from "../../app/ports/adapters/IEtherScanAdapter";

interface IConfig {
    apiKey?: string;
    urlParams?: string;
    url?: string;
}

export default class EtherScanAdapter {
    private config: IConfig;

    constructor(config: IConfig) {
        this.config = config;
    }

    isHexString(value: string): boolean {
        return ethers.isHexString(value);
    }

    async getTransactionByHash(txHash: string): Promise<IPreparedTransaction> {
        return this.send({ action: "eth_getTransactionByHash", txHash });
    }

    private async send(params) {
        try {
            const response = await fetch(this.prepareUrl(params));
            const transaction: any = await response.json();
            return this.prepareTransaction(transaction?.result);
        } catch (e: any) {
            throw new InfraError(e);
        }
    }

    private prepareUrl(params) {
        const { action, txHash } = params;
        return `${this.config.url}?${this.config.urlParams}&action=${action}&txhash=${txHash}&apikey=${this.config.apiKey}`;
    }

    private prepareTransaction(transaction) {
        return {
            txid: transaction.hash,
            value: this.prepareTransactionAmount(transaction),
            walletFrom: transaction.from,
            walletTo: transaction.to,
        };
    }

    private prepareTransactionAmount(transaction) {
        const dec = parseInt(transaction?.value, 16).toString();
        return `${ethers.formatEther(dec)} ETH`;
    }
}
