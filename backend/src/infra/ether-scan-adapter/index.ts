import EtherScanAdapter from "./EtherScanAdapter";

const etherScanAdapter = new EtherScanAdapter({
    apiKey: process.env.ETHERSCAN_API_KEY,
    urlParams: process.env.ETHERSCAN_URL_PARAMS,
    url: process.env.ETHERSCAN_URL,
});

export { etherScanAdapter };
