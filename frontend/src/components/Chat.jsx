import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { useLocation, useNavigate } from "react-router-dom";

import icon from "../images/emoji.svg";
import styles from "../styles/Chat.module.css";
import EmojiPicker from "emoji-picker-react";
import Messages from "./Messages";

const socket = io.connect("http://localhost:5000");

const Chat = () => {
    const navigate = useNavigate();
    const { search } = useLocation();
    const [params, setParams] = useState({ roomId: "", user: "" });
    const [state, setState] = useState([]);
    const [loggedUser, setLoggedUser] = useState({});
    const [usersCount, setUsers] = useState(0);
    const [userMessage, setMessage] = useState("");
    const [isOpen, setOpen] = useState(false);

    useEffect(() => {
        const searchParams = Object.fromEntries(new URLSearchParams(search));
        setParams(searchParams);
        socket.emit("join", searchParams);
    }, [search]);

    useEffect(() => {
        socket.on("message", ({ data }) => {
            setState((_state) => ([..._state, data]));
        });
    }, []);

    useEffect(() => {
        socket.on("updateUsers", ({ data }) => {
            if (data.loggedUser) {
                setLoggedUser(data.loggedUser);
            }
            console.log(data.users);
            if (data.users) {
                setUsers(data.users);
            }
        });
    }, []);


    const leftRoom = () => {
        socket.emit("leftRoom", { loggedUser });
        navigate("/");
    };

    const handleChange = ({ target: { value } }) => setMessage(value);

    const onEmojiClick = ({ emoji }) => setMessage(`${userMessage} ${emoji}`);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!userMessage) return;
        socket.emit("sendMessage", { userMessage, loggedUser });
        setMessage("");
    };

    return (
        <div className={styles.wrap}>
            <div className={styles.header}>
                <div className={styles.title}>
                    {params.roomId}
                </div>
                <div className={styles.users}>
                    {usersCount} users in this room
                </div>
                <button className={styles.left} onClick={leftRoom}>
                    Left the room
                </button>
            </div>

            <div className={styles.messages}>
                <Messages messages={state} name={params.name} />
            </div>

            <form className={styles.form} onSubmit={handleSubmit}>
                <div className={styles.input}>
                    <input
                        type="text"
                        name="message"
                        value={userMessage}
                        placeholder="What do you want write"
                        onChange={handleChange}
                        autoComplete="off"
                        required
                    />
                </div>
                <div className={styles.emoji}>
                    <img src={icon} alt="emoji" onClick={() => setOpen(!isOpen)} />
                    {isOpen && (
                        <div className={styles.emojies}>
                            <EmojiPicker onEmojiClick={onEmojiClick} />
                        </div>
                    )}
                </div>

                <div className={styles.button}>
                    <input type="submit" onSubmit={handleSubmit} value="Send a message" />
                </div>
            </form>
        </div>
    );
};

export default Chat;