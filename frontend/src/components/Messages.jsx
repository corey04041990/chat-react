import React from "react";
import styles from "../styles/Messages.module.css";

const Messages = ({ messages, name }) => {
    return (
        <div className={styles.messages}>
            {messages.map(({ user, message, transactionInfo }, i) => {
                const isMe = user.name.trim().toLowerCase() === name.trim().toLowerCase();
                const className = isMe ? styles.me : styles.user;
                return (
                    <div key={i} className={`${styles.messages} ${className}`}>
                        <span className={styles.user}>{user.name}</span>
                        <div className={styles.text}>{message}</div>
                        {transactionInfo && (
                            <>
                                <div className={styles.transaction}>Txid: {transactionInfo?.txid}</div>
                                <div className={styles.transaction}>Sum: {transactionInfo?.value}</div>
                                <div className={styles.transaction}>WalletFrom: {transactionInfo?.walletFrom}</div>
                                <div className={styles.transaction}>WalletTo: {transactionInfo?.walletTo}</div>
                            </>
                        )}

                    </div>
                );
            })}
        </div>
    );
};

export default Messages;