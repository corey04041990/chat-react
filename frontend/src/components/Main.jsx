import React, { useState } from "react";
import styles from "../styles/Main.module.css";
import { Link } from "react-router-dom";

const FIELDS = {
    NAME: "name",
    ROOM_ID: "roomId"
};

const Main = () => {
    const { NAME, ROOM_ID } = FIELDS;
    const [values, setValues] = useState({ [NAME]: "", [ROOM_ID]: "" });

    const handleChange = ({ target: { value, name } }) => {
        setValues({ ...values, [name]: value });
    };

    const handleClick = (e) => {
        const isDisabled = Object.values(values).some(value => !value);
        if (isDisabled) {
            e.preventDefault();
        }
    };
    return (
        <div className={styles.wrap}>
            <div className={styles.container}>
                <h1 className={styles.heading}>Join</h1>
                <form className={styles.form}>
                    <div className={styles.group}>
                        <input
                            type="text"
                            name="name"
                            value={values[NAME]}
                            placeholder="name"
                            className={styles.input}
                            onChange={handleChange}
                            autoComplete="off"
                            required
                        />
                    </div>
                    <div className={styles.group}>
                        <input
                            type="text"
                            name="roomId"
                            value={values[ROOM_ID]}
                            placeholder="Room"
                            className={styles.input}
                            onChange={handleChange}
                            autoComplete="off"
                            required
                        />
                    </div>
                    <Link
                        className={styles.group}
                        onClick={handleClick}
                        to={`/chat?${FIELDS.NAME}=${values[NAME]}&${FIELDS.ROOM_ID}=${values[ROOM_ID]}`}
                    >
                        <button type="submit" className={styles.button}>Sign In</button>
                    </Link>
                </form>
            </div>
        </div>
    );
};

export default Main;